//
//  AFContainerView.m
//  InscribedRect
//
//  Created by Cameron Spickert on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AFContainerView.h"

@implementation AFContainerView

@synthesize intersectionPoints = _intersectionPoints;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor redColor] CGColor]);
    
    CGRect bounds = [self bounds];
    CGPoint min = CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds));
    CGPoint mid = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    CGPoint max = CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
    
    CGContextMoveToPoint(context, min.x, min.y);
    CGContextAddLineToPoint(context, max.x, max.y);
    CGContextClosePath(context);
    
    CGContextMoveToPoint(context, max.x, min.y);
    CGContextAddLineToPoint(context, min.x, max.y);
    CGContextClosePath(context);
    
    CGContextSetStrokeColorWithColor(context, [[UIColor darkGrayColor] CGColor]);
    CGContextStrokePath(context);
    
    CGFloat rightX = CGFLOAT_MAX, bottomY = CGFLOAT_MAX;
    CGFloat leftX = CGFLOAT_MIN, topY = CGFLOAT_MIN;
    
    for (NSValue *pointValue in [self intersectionPoints]) {
        CGPoint point = [pointValue CGPointValue];
        if (point.y > mid.y) {
            if (point.y < bottomY) bottomY = point.y;
        } else {
            if (point.y > topY) topY = point.y;
        }
        if (point.x > mid.x) {
            if (point.x < rightX) rightX = point.x;
        } else {
            if (point.x > leftX) leftX = point.x;
        }
        CGContextFillEllipseInRect(context, CGRectMake(point.x - 3, point.y - 3, 6, 6));
    }
    
    CGRect fillRect = CGRectMake(leftX, topY, rightX - leftX, bottomY - topY);
    
    CGContextSetFillColorWithColor(context, [[UIColor blueColor] CGColor]);
    CGContextFillRect(context, fillRect);
    
//    CGFloat dist = CGFLOAT_MAX;
//    CGPoint p1, p2;
//    
//    for (NSValue *pointValue1 in [self intersectionPoints]) {
//        CGPoint point1 = [pointValue1 CGPointValue];
//        
//        for (NSValue *pointValue2 in [self intersectionPoints]) {
//            if (pointValue2 == pointValue1) continue;
//            
//            CGPoint point2 = [pointValue2 CGPointValue];
//            
//            CGFloat currDist = sqrtf(powf(point2.x - point1.x, 2) + powf(point2.y - point1.y, 2));
//            if (currDist < dist) {
//                p1 = point1, p2 = point2;
//                dist = currDist;
//            }
//        }
//    }
//    
//    CGContextFillEllipseInRect(context, CGRectMake(p1.x - 3, p1.y - 3, 6, 6));
//    CGContextFillEllipseInRect(context, CGRectMake(p2.x - 3, p2.y - 3, 6, 6));
}

@end
