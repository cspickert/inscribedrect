#include "CGLine.h"

bool CGLineIntersection(CGLine l1, CGLine l2, CGPoint *i)
{
	CGFloat x1 = l1.p1.x, x2 = l1.p2.x;
	CGFloat y1 = l1.p1.y, y2 = l1.p2.y;
	CGFloat x3 = l2.p1.x, x4 = l2.p2.x;
	CGFloat y3 = l2.p1.y, y4 = l2.p2.y;
	
	CGFloat m1 = (y2 - y1) / (x2 - x1);
	CGFloat m2 = (y4 - y3) / (x4 - x3);
	
	if (isinf(m1) && isinf(m2)) {
		// l1 and l2 are both vertical and either identical or parallel
		return false;
	} else if (isinf(m1)) {
		// l1 is vertical, l2 is not
		CGFloat b2 = y3 - (m2 * x3);
		*i = CGPointMake(x1, m2 * x1 + b2);
		return true;
	} else if (isinf(m2)) {
		// l2 is vertical, l1 is not
		CGFloat b1 = y1 - (m1 * x1);
		*i = CGPointMake(x3, m1 * x3 + b1);
		return true;
	} else if (m1 == m2) {
		// l1 and l2 are parallel
		return false;
	}
	
	CGFloat b1 = y1 - (m1 * x1);
	CGFloat b2 = y3 - (m2 * x3);
	
	CGFloat ix = -(b1 - b2) / (m1 - m2);
	CGFloat iy = m1 * ix + b1;
	
	*i = CGPointMake(ix, iy);
	
	return true;
}
