//
//  AFViewController.h
//  InscribedRect
//
//  Created by Cameron Spickert on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *outerView;
@property (strong, nonatomic) IBOutlet UIView *innerView;

@end
