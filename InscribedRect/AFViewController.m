//
//  AFViewController.m
//  InscribedRect
//
//  Created by Cameron Spickert on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AFViewController.h"
#import "AFBoxView.h"
#import "CGLine.h"
#import "AFContainerView.h"

@interface AFViewController ()

@end

@implementation AFViewController
@synthesize outerView;
@synthesize innerView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    CGRect frame = [[self view] bounds];
    frame = CGRectInset(frame, 50, 50 * CGRectGetHeight(frame) / CGRectGetWidth(frame));
    [[self outerView] setFrame:frame];
    
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
    [[self view] addGestureRecognizer:rotationRecognizer];
}

- (void)viewDidUnload
{
    [self setOuterView:nil];
    [self setInnerView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)handleRotation:(UIRotationGestureRecognizer *)recognizer
{
    CGFloat rotation = [recognizer rotation];
    CGRect bounds = [[self outerView] bounds];
    bounds = CGRectInset(bounds, 1.0, 1.0 * (CGRectGetHeight(bounds) / CGRectGetWidth(bounds)));
    
    [[self outerView] setTransform:CGAffineTransformMakeRotation(rotation)];
    
    CGPoint min = CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds));
    CGPoint mid = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    CGPoint max = CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
    
    CGPoint tl = min, tr = CGPointMake(max.x, min.y);
    CGPoint bl = CGPointMake(min.x, max.y), br = max;
    
    min = [[self view] convertPoint:min fromView:[self outerView]];
    mid = [[self view] convertPoint:mid fromView:[self outerView]];
    max = [[self view] convertPoint:max fromView:[self outerView]];
    
    tl = [[self view] convertPoint:tl fromView:[self outerView]];
    tr = [[self view] convertPoint:tr fromView:[self outerView]];
    bl = [[self view] convertPoint:bl fromView:[self outerView]];
    br = [[self view] convertPoint:br fromView:[self outerView]];
    
    CGLine rectEdges[4] = {
        { tl, tr },
        { tr, br },
        { br, bl },
        { bl, tl }
    };
    
    bounds = [[self view] bounds];
    
    min = CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds));
    max = CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));

    CGLine line1 = { CGPointMake(min.x, min.y), CGPointMake(max.x, max.y) };
    CGLine line2 = { CGPointMake(max.x, min.y), CGPointMake(min.x, max.y) };
    
    NSMutableArray *intersectionPointValues = [NSMutableArray array];
    
    for (int i = 0 ; i < 4; i++) {
        CGPoint intersectionPoint, convertedIntersectionPoint;
        CGLine l1 = rectEdges[i];
        if (CGLineIntersection(l1, line1, &intersectionPoint)) {
            convertedIntersectionPoint = [[self view] convertPoint:intersectionPoint toView:[self outerView]];
            if (CGRectContainsPoint([[self outerView] bounds], convertedIntersectionPoint)) {
                [intersectionPointValues addObject:[NSValue valueWithCGPoint:intersectionPoint]];
            }
        }
        if (CGLineIntersection(l1, line2, &intersectionPoint)) {
            convertedIntersectionPoint = [[self view] convertPoint:intersectionPoint toView:[self outerView]];
            if (CGRectContainsPoint([[self outerView] bounds], convertedIntersectionPoint)) {
                [intersectionPointValues addObject:[NSValue valueWithCGPoint:intersectionPoint]];
            }
        }
    }
    
    AFContainerView *conView = (id)[self view];
    [conView setIntersectionPoints:intersectionPointValues];
    [conView setNeedsDisplay];
}

@end
