//
//  AFAppDelegate.h
//  InscribedRect
//
//  Created by Cameron Spickert on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AFViewController;

@interface AFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AFViewController *viewController;

@end
