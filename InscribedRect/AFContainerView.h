//
//  AFContainerView.h
//  InscribedRect
//
//  Created by Cameron Spickert on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFContainerView : UIView

@property (nonatomic, strong) NSArray *intersectionPoints;

@end
