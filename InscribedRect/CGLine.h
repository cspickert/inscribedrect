#ifndef CGLINE_H_
#define CGLINE_H_

#include <QuartzCore/QuartzCore.h>

struct CGLine {
	CGPoint p1, p2;
};
typedef struct CGLine CGLine;

static inline CGLine __CGLineMake(CGPoint p1, CGPoint p2) {
	return (CGLine){ .p1 = p1, .p2 = p2 };
}
#define CGLineMake __CGLineMake

bool CGLineIntersection(CGLine l1, CGLine l2, CGPoint *i);

#endif
