//
//  AFBoxView.m
//  InscribedRect
//
//  Created by Cameron Spickert on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AFBoxView.h"

@implementation AFBoxView

@synthesize rotation = _rotation;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setRotation:(CGFloat)rotation
{
    _rotation = rotation;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGPoint min = CGPointMake(CGRectGetMinX([self bounds]), CGRectGetMinY([self bounds]));
    CGPoint mid = CGPointMake(CGRectGetMidX([self bounds]), CGRectGetMidY([self bounds]));
    CGPoint max = CGPointMake(CGRectGetMaxX([self bounds]), CGRectGetMaxY([self bounds]));
    
    CGPoint crossLines[2][2] = {
        { CGPointMake(min.x, min.y), CGPointMake(max.x, max.y) },
        { CGPointMake(max.x, min.y), CGPointMake(min.x, max.y) }
    };
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for (int i = 0; i < 2; i++) {
        CGPoint *line = crossLines[i];
        CGContextMoveToPoint(context, line[0].x, line[0].y);
        CGContextAddLineToPoint(context, line[1].x, line[1].y);
        CGContextClosePath(context);
    }
    
    CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
    CGContextStrokePath(context);
    
    CGContextTranslateCTM(context,  mid.x,  mid.y);
    CGContextRotateCTM(context, [self rotation]);
    CGContextScaleCTM(context, 0.8f, 0.8f);
    CGContextTranslateCTM(context, -mid.x, -mid.y);
    
    CGContextSetFillColorWithColor(context, [[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.4] CGColor]);
    CGContextFillRect(context, CGRectMake(min.x, min.y, max.x, max.y));
    
    CGPoint rectEdges[4][2] = {
        { CGPointMake(min.x, min.y), CGPointMake(max.x, min.y) },
        { CGPointMake(max.x, min.y), CGPointMake(max.x, max.y) },
        { CGPointMake(max.x, max.y), CGPointMake(min.x, max.y) },
        { CGPointMake(min.x, max.y), CGPointMake(min.x, min.y) }
    };
}

@end
