//
//  AFBoxView.h
//  InscribedRect
//
//  Created by Cameron Spickert on 3/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFBoxView : UIView

@property (nonatomic, assign) CGFloat rotation;

@end
